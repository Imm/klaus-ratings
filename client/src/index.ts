import { credentials, loadPackageDefinition } from 'grpc'
import { loadSync } from '@grpc/proto-loader'

const protoPath = __dirname + '/../../api/protos/ratings.proto'

const packageDefinition = loadSync(
  protoPath,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  }
)

const ratings: any = loadPackageDefinition(packageDefinition).ratings

const client = new ratings.Ratings(
  'localhost:50051',
  credentials.createInsecure()
)

const startTime = new Date()
const endTime = new Date()

startTime.setDate(startTime.getDate() - 12)
startTime.setUTCHours(0, 0, 0, 0);

endTime.setDate(endTime.getDate() - 9)
endTime.setUTCHours(23, 59, 59, 999)

function logAggregatedCategories() {
  const call = client.getAggregatedCategories({
    startTime: startTime.toISOString(),
    endTime: endTime.toISOString()
  })

  call.on('data', (category) => {
    console.log(category)
  })
}

function logAggregatedTickets() {
  const call = client.getAggregatedTickets({
    startTime: startTime.toISOString(),
    endTime: endTime.toISOString()
  })

  call.on('data', (ticket) => {
    console.log(ticket)
  })
}

function logOverallQualityScore() {
  client.getOverallQualityScore({
    startTime: startTime.toISOString(),
    endTime: endTime.toISOString()
  }, (error, score) => {
    console.log(score)
  })
}

function logScoreChangeOverPeriods() {
  client.getScoreChangeOverPeriods({
    startTime: startTime.toISOString(),
    endTime: endTime.toISOString()
  }, (error, changePercentage) => {
    console.log(changePercentage)
  })
}

// logAggregatedCategories()
// logAggregatedTickets()
// logOverallQualityScore()
logScoreChangeOverPeriods()