# Klaus Ratings

- gRPC service methods take in whatever time range in ISO 8601 format
- Period over period score change will compare your selected time range and a period with the same length that ends with your selected start time
- Category weights are considered only in overall quality score calculation. For me it didn't make sense to use it anywhere else or I have misunderstood the weight concept, I'd be happy to discuss that topic.

### Get the service running

`$ cd api/`

`$ npm install`

`$ npm start`

gRPC server will be running on localhost:50051

### Get the client running

`$ cd client/`

`$ npm install`

`$ npm start`

Client code in src/index.ts should be changed to run different calls or to change the time range

### Testing

`$ cd api/`

`$ npm test`

Only aggregation methods are tested

### Deployment

Service should be more configurable and production building part is kind of missing. I usually use Docker for development environment and for production aswell, didn't add any Dockerization here as it seemed unnecessary. For container orchestration I would go with Kubernetes.