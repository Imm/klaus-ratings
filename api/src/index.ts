import DatabaseService from './services/database'
import RatingsService from './services/ratings'
import AggregationService from './services/aggregation'
import GRPCService from './services/grpc'

const db = new DatabaseService()
const ratingsService = new RatingsService(db)
const aggregationService = new AggregationService()
const gRPCService = new GRPCService(ratingsService, aggregationService)