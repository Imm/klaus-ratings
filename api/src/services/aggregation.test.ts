import 'mocha';
import { expect } from 'chai'
import { IRating } from './ratings'
import AggregationService from './aggregation'

describe('Aggregation service', function() {
  const aggregationService = new AggregationService()

  it('aggregates daily scores by category', function() {
    const ratings: IRating[] = [
      {
        id: 1,
        rating: 3,
        ticket_id: 1,
        created_at: '2020-03-02T15:55:21',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      },
      {
        id: 2,
        rating: 5,
        ticket_id: 1,
        created_at: '2020-03-02T17:44:05',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      },
      {
        id: 3,
        rating: 5,
        ticket_id: 2,
        created_at: '2020-03-03T12:23:14',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      },
      {
        id: 4,
        rating: 2,
        ticket_id: 2,
        created_at: '2020-03-02T19:02:53',
        category_id: 2,
        category_name: 'Random',
        category_weight: 0
      }
    ]

    const categories = aggregationService.aggregateDailyScoresByCategory(ratings)

    expect(categories).to.have.length(2)
    expect(categories[0].dateScores).to.have.length(2)
    expect(categories[0].dateScores[0].score).equal(0.8)
  })

  it('aggregates category scores by ticket', function() {
    const ratings: IRating[] = [
      {
        id: 1,
        rating: 3,
        ticket_id: 1,
        created_at: '2020-03-02T15:55:21',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      },
      {
        id: 1,
        rating: 4,
        ticket_id: 1,
        created_at: '2020-03-02T16:55:21',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      },
      {
        id: 2,
        rating: 5,
        ticket_id: 1,
        created_at: '2020-03-02T17:44:05',
        category_id: 3,
        category_name: 'GDPR',
        category_weight: 1.2
      },
      {
        id: 3,
        rating: 5,
        ticket_id: 2,
        created_at: '2020-03-03T12:23:14',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 0.7
      }
    ]

    const tickets = aggregationService.aggregateCategoryScoresByTicket(ratings)

    expect(tickets).to.have.length(2)
    expect(tickets[0].categoryScores).to.have.length(2)
    expect(tickets[0].categoryScores[0].score).equal(0.7)
  })

  it('calculates overall quality score', function() {
    const ratings: IRating[] = [
      {
        id: 1,
        rating: 4,
        ticket_id: 1,
        created_at: '2020-03-02T15:55:21',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 1
      },
      {
        id: 1,
        rating: 4,
        ticket_id: 1,
        created_at: '2020-03-02T16:55:21',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 1
      },
      {
        id: 2,
        rating: 1,
        ticket_id: 1,
        created_at: '2020-03-02T17:44:05',
        category_id: 3,
        category_name: 'GDPR',
        category_weight: 1.5
      },
      {
        id: 3,
        rating: 4,
        ticket_id: 2,
        created_at: '2020-03-03T12:23:14',
        category_id: 1,
        category_name: 'Grammar',
        category_weight: 1
      }
    ]

    const overallScore = aggregationService.overallQualityScore(ratings)

    expect(overallScore).equal(0.6)
  })
})