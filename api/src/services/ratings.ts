import DatabaseService from './database'

export const maxRating = 5

export interface IRating {
  id: number
  rating: number
  ticket_id: number
  created_at: string
  category_id: number
  category_name: string
  category_weight: number
}

export default class RatingsService {
  private db: DatabaseService

  constructor(db: DatabaseService) {
    this.db = db
  }

  async getByTimeRange(startTime: string, endTime: string): Promise<IRating[]> {
    const ratings: IRating[] = await this.db.query(`
      SELECT
        r.id,
        r.rating,
        r.ticket_id,
        r.created_at,
        rc.id AS category_id,
        rc.name AS category_name,
        rc.weight AS category_weight
      FROM ratings r
      LEFT JOIN rating_categories rc
      ON rc.id = r.rating_category_id
      WHERE created_at >= ? AND created_at <= ?
      ORDER BY r.created_at
    `, [startTime, endTime])

    return ratings
  }
}