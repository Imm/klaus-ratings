import { IRating, maxRating } from './ratings'

export interface IDateScore {
  date: string
  score: number
  ratings: number
  ratingSum: number
}

export interface ICategory {
  id: number
  name: string
  ratings: number
  score: number
  dateScores: IDateScore[]
}

export interface ICategoryScore {
  id: number
  name: string
  score: number
  ratings: number
  ratingSum: number
}

export interface ITicket {
  id: number
  categoryScores: ICategoryScore[]
}

export default class AggregationService {
  aggregateDailyScoresByCategory(ratings: IRating[]): ICategory[] {
    const categories: ICategory[] = []

    ratings.forEach((rating: IRating) => {
      let category: ICategory | undefined = categories.find((category: ICategory) => category.id === rating.category_id)

      if (!category) {
        category = {
          id: rating.category_id,
          name: rating.category_name,
          ratings: 0,
          score: 0,
          dateScores: []
        }

        categories.push(category)
      }

      category.ratings++

      const ratingDate = rating.created_at.split('T')[0]
      let dateScore: IDateScore | undefined = category.dateScores.find((dateScore: IDateScore) => dateScore.date === ratingDate)

      if (!dateScore) {
        dateScore = {
          date: ratingDate,
          score: 0,
          ratings: 0,
          ratingSum: 0,
        }

        category.dateScores.push(dateScore)
      }

      dateScore.ratings++
      dateScore.ratingSum += rating.rating
      dateScore.score = dateScore.ratingSum / dateScore.ratings / maxRating

      let categoryRatings = 0
      let categoryRatingSum = 0

      category.dateScores.forEach((dateScore: IDateScore) => {
        categoryRatings += dateScore.ratings
        categoryRatingSum += dateScore.ratingSum
      })

      category.score = categoryRatingSum / categoryRatings / maxRating
    })

    return categories
  }

  aggregateCategoryScoresByTicket(ratings: IRating[]): ITicket[] {
    const tickets: ITicket[] = []

    ratings.forEach((rating: IRating) => {
      let ticket: ITicket | undefined = tickets.find((ticket: ITicket) => ticket.id === rating.ticket_id)

      if (!ticket) {
        ticket = {
          id: rating.ticket_id,
          categoryScores: []
        }

        tickets.push(ticket)
      }

      let categoryScore: ICategoryScore | undefined = ticket.categoryScores.find((categoryScore: ICategoryScore) => categoryScore.id === rating.category_id)

      if (!categoryScore) {
        categoryScore = {
          id: rating.category_id,
          name: rating.category_name,
          score: 0,
          ratings: 0,
          ratingSum: 0
        }

        ticket.categoryScores.push(categoryScore)
      }

      categoryScore.ratings++
      categoryScore.ratingSum += rating.rating
      categoryScore.score = categoryScore.ratingSum / categoryScore.ratings / maxRating
    })

    return tickets
  }

  overallQualityScore(ratings: IRating[]): number {
    let ratingsLength = 0
    let ratingSum = 0

    ratings.forEach((rating: IRating) => {
      ratingsLength += 1 * rating.category_weight
      ratingSum += rating.rating * rating.category_weight
    })

    const score = (ratingSum / ratingsLength) / maxRating

    return score
  }
}