import { Database } from 'sqlite3'

const dbFile = './database.db'
const db = new Database(dbFile)

export default class DatabaseService {
  query = (query: string, params: string[] = []) => new Promise(
    (resolve: (rows: any[]) => void, reject) => {
      db.all(query, params, (error, rows) => {
        if (error) {
          return reject(error)
        }
    
        resolve(rows)
      })
    }
  )
}