import { Server, ServerCredentials, loadPackageDefinition } from 'grpc'
import { loadSync } from '@grpc/proto-loader'
import RatingsService from './ratings'
import AggregationService from './aggregation'

const protoPath = __dirname + '/../../protos/ratings.proto'

const packageDefinition = loadSync(
  protoPath,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  }
)

const ratings: any = loadPackageDefinition(packageDefinition).ratings

export default class GRPC {
  private ratingsService: RatingsService
  private aggregationService: AggregationService

  server: Server | null = null

  constructor(ratingsService: RatingsService, aggregationService: AggregationService) {
    this.ratingsService = ratingsService
    this.aggregationService = aggregationService

    this.getServer()
  }

  getAggregatedCategories = async (call: any) => {
    const ratings = await this.ratingsService.getByTimeRange(call.request.startTime, call.request.endTime)
    const categories = this.aggregationService.aggregateDailyScoresByCategory(ratings)

    categories.forEach((category) => {
      call.write(category)
    })

    call.end()
  }

  getAggregatedTickets = async (call: any) => {
    const ratings = await this.ratingsService.getByTimeRange(call.request.startTime, call.request.endTime)
    const tickets = this.aggregationService.aggregateCategoryScoresByTicket(ratings)

    tickets.forEach((ticket) => {
      call.write(ticket)
    })

    call.end()
  }

  getOverallQualityScore = async (call: any, callback: any) => {
    const ratings = await this.ratingsService.getByTimeRange(call.request.startTime, call.request.endTime)
    const score = this.aggregationService.overallQualityScore(ratings)

    callback(null, { score })
  }

  getScoreChangeOverPeriods = async (call: any, callback: any) => {
    const currentStartTime = new Date(call.request.startTime)
    const currentEndTime = new Date(call.request.endTime)

    const previousStartTime = new Date(call.request.startTime)
    const previousEndTime = new Date(call.request.endTime)

    const timeDifference = currentEndTime.getTime() - currentStartTime.getTime() + 1

    previousStartTime.setTime(previousStartTime.getTime() - timeDifference)
    previousEndTime.setTime(previousEndTime.getTime() - timeDifference)

    const previousRatings = await this.ratingsService.getByTimeRange(previousStartTime.toISOString(), previousEndTime.toISOString())
    const previousScore = this.aggregationService.overallQualityScore(previousRatings)

    const currentRatings = await this.ratingsService.getByTimeRange(currentStartTime.toISOString(), currentEndTime.toISOString())
    const currentScore = this.aggregationService.overallQualityScore(currentRatings)

    const changePercentage = currentScore / previousScore - 1

    callback(null, { changePercentage })
  }

  getServer() {
    const server = new Server()

    server.addService(ratings.Ratings.service, {
      getAggregatedCategories: this.getAggregatedCategories,
      getAggregatedTickets: this.getAggregatedTickets,
      getOverallQualityScore: this.getOverallQualityScore,
      getScoreChangeOverPeriods: this.getScoreChangeOverPeriods
    })

    server.bind('0.0.0.0:50051', ServerCredentials.createInsecure())
    server.start()

    this.server = server

    return server
  }
}